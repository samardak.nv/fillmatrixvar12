package com.main

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows

class FillMatrixTest {
    @Test
    fun `size 8 full test`() {
        var testedArray: SquareMatrix = FillMatrix.fillMatrix(SquareMatrixBuilder.build(8))
        val correctArray: Array<Array<Int>> = Array(8) { Array(8) { 0 } }
        correctArray[0] = arrayOf(22, 21, 11, 10, 4, 3, 1, 36)
        correctArray[1] = arrayOf(23, 20, 12, 9, 5, 2, 35, 37)
        correctArray[2] = arrayOf(24, 19, 13, 8, 6, 34, 49, 38)
        correctArray[3] = arrayOf(25, 18, 14, 7, 33, 50, 48, 39)
        correctArray[4] = arrayOf(26, 17, 15, 32, 58, 51, 47, 40)
        correctArray[5] = arrayOf(27, 16, 31, 59, 57, 52, 46, 41)
        correctArray[6] = arrayOf(28, 30, 63, 60, 56, 53, 45, 42)
        correctArray[7] = arrayOf(29, 64, 62, 61, 55, 54, 44, 43)

        assertEquals(6, testedArray.getCell(2, 4))
        assertEquals(31, testedArray.getCell(5, 2))
        assertEquals(9, testedArray.getCell(1, 3))
        assertEquals(27, testedArray.getCell(5, 0))
        assertEquals(22, testedArray.getCell(0, 0))
        assertEquals(49, testedArray.getCell(2, 6))
    }

    @Test
    fun `size 5 full test`() {
        var testedArray: SquareMatrix = FillMatrix.fillMatrix(SquareMatrixBuilder.build(5))
        val correctArray: Array<Array<Int>> = Array(5) { Array(5) { 0 } }
        correctArray[0] = arrayOf(10, 4, 3, 1, 15)
        correctArray[1] = arrayOf(9, 5, 2, 14, 16)
        correctArray[2] = arrayOf(8, 6, 13, 22, 17)
        correctArray[3] = arrayOf(7, 12, 23, 21, 18)
        correctArray[4] = arrayOf(11, 25, 24, 20, 19)

        assertEquals(25, testedArray.getCell(4, 1))
        assertEquals(8, testedArray.getCell(2, 0))
        assertEquals(16, testedArray.getCell(1, 4))
        assertEquals(5, testedArray.getCell(1, 1))
        assertEquals(3, testedArray.getCell(0, 2))
        assertEquals(19, testedArray.getCell(4, 4))
    }

    @Test
    fun `size 1 full test`() {
        var testedArray: SquareMatrix = FillMatrix.fillMatrix(SquareMatrixBuilder.build(1))
        val correctArray: Array<Array<Int>> = Array(1) { Array(1) { 0 } }
        correctArray[0][0] = 1

        assertEquals(1, testedArray.getCell(0, 0))
    }

    @Test
    fun `size 0 full test`() {
        assertThrows(IllegalArgumentException::class.java) { FillMatrix.fillMatrix(SquareMatrixBuilder.build(0)) }
    }
}
