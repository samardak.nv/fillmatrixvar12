package com.main

object SquareMatrixBuilder {
    /**
     * builds SquareMatrix
     * @param size - size of square matrix
     * */
    fun build( size: Int ): SquareMatrix {
        return if ( size <= 0 ) {
            throw IllegalArgumentException("array can't be zero size")
        } else {
            SquareMatrix( size )
        }
    }
}