package com.main

import kotlin.math.abs
import kotlin.math.ceil
import kotlin.math.sqrt

object FillMatrix {


    /**
     * Calculates Y - column of the matrix, according to the pattern
     * @param i Value
     * @param n Size of matrix
     */

    private fun getY(i: Int, n: Int): Int {
        var y = 0

        if ((i >= 0) && (i < ((n * n) - n) / 2)) {
            y = n - 2 - ceil(((sqrt((8 * i + 2).toDouble()) - 3) / 2)).toInt()
        }
        if ((i >= (n * n - n) / 2 ) && (i < ((n * n - n) / 2) + n)) {
            y = abs(i - (n * n - n) / 2)
        }
        if ( ((i >= ((n * n - n) / 2) + n) ) && (i <= (n * n - 1))) {
            y = abs((n - 2 - ceil(((sqrt((8 * abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) + 2).toDouble()) - 3) / 2)).toInt()) - n + 1)
        }
        return (y)
    }


    /**
     * Calculates X - line of the matrix, according to the pattern
     * @param i Value
     * @param n Size of matrix
     */

    private fun getX(i: Int, n: Int): Int {
        var x = 0
        var y: Int
        val f: Int

        if ((i >= 0) && (i < ((n * n) - n) / 2)) {
            y = n - 2 - ceil(((sqrt((8 * i + 2).toDouble()) - 3) / 2)).toInt()
            x = if ((n - y) % 2 == 0){
                (i - (( (n - y - 1) / 2) * (( (n - y - 1)))))
            } else {
                abs(i - (((n - y) / 2) * (((n - y)))) + 1)
            }
        }
        if ((i >= (n * n - n) / 2 ) && (i < ((n * n - n) / 2) + n)) {
            y = abs(i - (n * n - n) / 2)
            x = abs(n - y - 1)
        }
        if ( ((i >= ((n * n - n) / 2) + n) ) && (i <= (n * n - 1))) {
            y = abs((n - 2 - ceil(((sqrt((8 * abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) + 2).toDouble()) - 3) / 2)).toInt()) - n + 1)
            f = n - 2 - ceil(((sqrt((8 * abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) + 2).toDouble()) - 3) / 2)).toInt()
            x = if (n % 2 == 0) {
                if ((n - y) % 2 == 0) {
                    abs(abs(abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) - (((n - f) / 2) * (((n - f)))) + 1) - n) - 1
                } else {
                    abs((abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) - (((n - f - 1) / 2) * (((n - f - 1))))) - n) - 1
                }
            } else {
                if ((n - y) % 2 == 0) {
                    abs(abs(abs((abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) - (((n - f - 1) / 2) * (((n - f - 1))))) - n) - 1) - n) + abs(n - 1 - y)
                } else {
                    abs(abs(abs(abs(abs((i - (((n * n - n) / 2) + n) + 1) - ((n * n - n) / 2)) - (((n - f) / 2) * (((n - f)))) + 1) - n) - 1) - n) + abs(n - 1 - y)
                }
            }
        }
        return (x)
    }


    /**
    * fillMatrix fills squareMatrix with pattern from 12 var
    * @param arr matrix of type squareMatrix
    * */
    fun fillMatrix (arr: SquareMatrix): SquareMatrix{
        IntRange(0, arr.getSize()*arr.getSize()-1).forEach {
            arr.setCell( getX ( it, arr.getSize() ) , getY( it, arr.getSize() ) , it + 1)
        }
        return (arr)
    }
}
