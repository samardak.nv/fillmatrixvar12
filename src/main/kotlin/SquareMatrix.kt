package com.main

/**
 * SquareMatrix class, square array
 * @param _size - size of square matrix
 * */

class SquareMatrix (var _size: Int){
    private var size = _size
    private var array: Array<Array<Int>> = Array(size) { Array(size) { 0 } }
     /**
     * getSize
     * @return size of matrix
     * */
    fun getSize(): Int { return size }
    /**
     * getCell
     * @param x
     * @param y
     * @return [x][y] array cell
     * */
    fun getCell(x: Int, y: Int): Int { return array[x][y] }
    /**
     * setCell rewrites cell with new value
     * @param x
     * @param y
     * @param value
     * */
    fun setCell(x: Int, y: Int, value: Int) { array[x][y] = value }

}
